-- Create Database 
CREATE DATABASE online_store;

-- Create products table (this is a master data)
CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(40) NOT NULL,
  `category` varchar(40) NOT NULL
);

-- Add primary key for the products table
ALTER TABLE `products` ADD PRIMARY KEY (`product_id`);

-- Add product id as auto increment value
ALTER TABLE `products` MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT;

-- Create product_details for maintaining stock and mrp (MRP and stock may change based on some factors ). Product_id is foreign key here.
CREATE TABLE `product_details` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `image` varchar(100) NOT NULL,
  `stock` int(11) NOT NULL,
  `is_active` tinyint(1) NOT NULL
);

-- Add primary key for the product_details table
ALTER TABLE `product_details` ADD PRIMARY KEY (`id`), ADD KEY `product_id` (`product_id`);

-- Add id as auto increment value
ALTER TABLE `product_details` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

-- Add relation between products and product_details
ALTER TABLE `product_details` ADD CONSTRAINT `product_details_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`product_id`);