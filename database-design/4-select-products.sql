-- Select Data with joins
select * from `product_details` left join `products` on product_details.product_id = products.product_id where product_details.is_active=true;

-- find products with cost below 10000
select * from `product_details` left join `products` on product_details.product_id = products.product_id where product_details.is_active=true and product_details.price < 10000