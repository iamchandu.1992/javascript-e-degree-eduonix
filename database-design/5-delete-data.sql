-- for delete product we have 2 solutions 1-soft delete (update is_active to false) 2- hard delete

-- soft delete
UPDATE `product_details` SET `is_active`=false WHERE `id`=1;

-- hard delete
DELETE FROM `product_details` WHERE `product_details`.`id` = 1;