//Bridge between API and Front End
const postDocument = $(document)

const helpers = {
    

    postsView: evt => {
        get('/all/posts')
        .then(response => response.json())
        .then(data => {
            let s = '';
            for(var i = 0; i < data.length; i++){
                s+="<div class='col-4 mt-2'><div class='card card-info'><div class='card-body'>";
                s+="<h2>"+data[i].title+"</h2><p class='text-right'>"+new Date(data[i].created_at)+"</p><hr/><p>"+data[i].body+"</p></div></div></div>";
            }
            if(s==''){
                    s="<div class='col-sm-12'><div class='alert alert-danger'>No Posts Found.</div></div>";
            }
            document.getElementById('all-posts').innerHTML = s;
        })
    },
}

postDocument.ready(helpers.postsView)


function get(path, data){
    return window.fetch(path, {
        method:'GET',
    })
}
    
    