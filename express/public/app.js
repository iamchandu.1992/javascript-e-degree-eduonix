//Bridge between API and Front End
const postDocument = $(document)
const CreatePost = document.querySelector('.CreatePost')

//Make post request and add note to database
CreatePost.addEventListener('submit', (e) => {
    e.preventDefault()
    const title = CreatePost.querySelector('.title').value
    const body = CreatePost.querySelector('.body').value
    post('/post', {title, body})
    .then(status => {
        helpers.loadPosts()
        alert('Post successfully added');
        CreatePost.querySelector('.title').value = '';
        CreatePost.querySelector('.body').value = '';
    })
})

const helpers = {
    loadPosts: evt => {
        get('/all/posts')
        .then(response => response.json())
        .then(data => {
            $('#body_data').empty()
            $('.title').empty()
            $('.body').empty()
            for(var i = 0; i < data.length; i++){
                $('#body_data').append($('<tr>')
                .append($('<td>')
                .text(data[i].id)
                )
                .append($('<td>')
                .text(new Date(data[i].created_at))
                )
                .append($('<td>')
                .text(data[i].title)
                )
                .append($('<td>')
                .text(data[i].body)
                )
                .append($('<td>')
                    .append($('<button>')
                    .attr('class', 'btn-danger btn')
                    .attr('onclick', 'deleteItem('+data[i].id+')')
                    .attr('id', data[i].id) 
                    .text('Delete')
                )))
            }
        })
    },

    postsView: evt => {
        get('/all/posts')
        .then(response => response.json())
        .then(data => {
            let s = '';
            for(var i = 0; i < data.length; i++){
                s+="<div class='col-4'><div class='card card-info'><div class='card-body'>";
                s+="<h2>"+data[i].title+"</h2><p class='text-right'>"+new Date(data[i].created_at)+"</p><hr/><p>"+data[i].body+"</p></div></div></div>";
            }
            document.getElementById('#all-posts').innerHTML = s;
        })
    },
}

postDocument.ready(helpers.loadPosts)

function deleteItem(item_id){
    deleteMethod('/post/'+item_id, {})
    .then(status => {
        helpers.loadPosts()
        alert('Post successfully delete')
    })
}

function get(path, data){
    return window.fetch(path, {
        method:'GET',
    })
}
    
    function post(path, data){
        return window.fetch(path, {
          method:'POST',
          headers: {
             'Accept':'application/json',
             'Content-Type':'application/json' 
          },
          body: JSON.stringify(data) 
        })
    }

function deleteMethod(path, data){
    return window.fetch(path, {
       method:'DELETE',
       headers:{
       'Accept':'application/json',
       'Content-Type':'application/json'
       },
       body: JSON.stringify(data)
    })
}