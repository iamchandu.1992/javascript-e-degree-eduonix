const store = require('../store')

module.exports = function(app, db){
    
    app.get('/all/posts', (req, res, next) => {
        store.getAllPosts()
        .then((result) => res.json(result))
    })

    app.get('/post/:id', (req, res, next) => {
        store.getPost({
            id: req.params.id,
        })
        .then((result) => res.json(result))
    })

    
    app.delete('/post/:id', (req, res, next) => {
        store.deletePost({
            id: req.params.id,
        })
        .then((result) => res.sendStatus(200))
    })

    
    app.put('/post/:id', (req, res, next) => {
        store.updatePost({
            id: req.params.id,
            title: req.body.title,
            body: req.body.body
        })
        .then((result) => res.sendStatus(200))
    })

    
    app.post('/post', (req, res, next) => {
        store.createPost({
            title: req.body.title,
            body: req.body.body
        })
        .then((result) => res.sendStatus(200))
    })

}