//this is the file used to make mysql queries

const knex = require('knex')(require('./knexfile'))

module.exports = {

    getAllPosts() {
        return knex.select('*').from('posts')
    },

    updatePost({id, title, body}){
        return knex('posts').where({id:id}).update({title:title, body:body})
    },

    getPost({id}){
        return knex.select('*').from('posts').where('id', id)
    },

    deletePost({id}){
        return knex('posts').where({id:id}).del()
    },

    createPost({title, body}){
        return knex('posts').insert({
            title, body
        })
    }
}