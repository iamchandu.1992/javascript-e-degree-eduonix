const express = require('express')
//body parser is used for passing in IDs to the URL
const bodyParser = require('body-parser')
//the path variable is used for absolute paths of html files (as needed)
var path = require('path')
const app = express()

//loading static files
app.use(express.static('public'))
app.use(bodyParser.json())

require('./routes/post-router')(app, {})

//this route is used to open a seperate file or details of a note and edit it
app.get('/posts', function(req, res){
    res.sendFile(path.join(__dirname + '/public/create-post.html'))
})

app.listen(3000, () => {
    console.log('Server is running on http://localhost:3000')
})
