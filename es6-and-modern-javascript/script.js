function validate() {
        const email = document.getElementById("mail").value;
        const password = document.getElementById("password").value;
        const conform_password = document.getElementById("con_password").value;
        const uname = document.getElementById("uname").value;
        if(email !='' && password != '' && conform_password!='' && uname!='') {
                document.getElementById("alrt").innerHTML = '';
                if(!validatAtleastSix(uname)) {
                        document.getElementById("alrt").innerHTML = '<div class="alert alert-danger">User name contains atleast 6 characters.</div>';
                } else if(!validateEmail(email)) {
                        document.getElementById("alrt").innerHTML = '<div class="alert alert-danger">Please Enter valid Emial.</div>';
                } else if(!validatAtleastSix(password)) {
                        document.getElementById("alrt").innerHTML = '<div class="alert alert-danger">password contains atleast 6 characters.</div>';
                } else if(!matchTexts(password, conform_password)) {
                        document.getElementById("alrt").innerHTML = '<div class="alert alert-danger">password and conform password are not equal.</div>';
                } else {
                        document.getElementById("alrt").innerHTML = '<div class="alert alert-success">Success</div>';
                }
                
        } else {
                if(uname =='')
                        document.getElementById("alrt").innerHTML = '<div class="alert alert-danger">Please fill User Name.</div>';
                else if(email =='')
                        document.getElementById("alrt").innerHTML = '<div class="alert alert-danger">Please fill Emial.</div>';
                else if(password == '')
                        document.getElementById("alrt").innerHTML = '<div class="alert alert-danger">Please fill Password.</div>';
                else
                        document.getElementById("alrt").innerHTML = '<div class="alert alert-danger">Please fill Conform Password.</div>';  
        }
}

validateEmail = (email) => {
        return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
}

validatAtleastSix = (value) => {
        return value.length>=6 ? true : false;
}

matchTexts = (text1, text2) => {
        return text1 === text2;
}