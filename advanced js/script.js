function generateRandom() {
        const randomArray = [];
        for(var i=0;i<20;i++) {
                randomArray.push(Math.floor(Math.random() * 100));
        }
        document.getElementById("randomarraydisplay").innerHTML = randomArray.join();
        document.getElementById("maxValueInarray").innerHTML = Math.max.call(null,...randomArray);
        document.getElementById("minValueInarray").innerHTML = (Math.min.bind(null, ...randomArray))();
}

function calVolume()
 {
  var radius = document.getElementById('radius').value;
  if(radius>0) {
        radius = Math.abs(radius);
        var volume = (4/3) * Math.PI * Math.pow(radius, 3);
        volume = volume.toFixed(4);
        document.getElementById('volume').innerHTML = volume;
  } else {
          alert('please enter valid details.');
  }
  
 } 

function displayDetails() {
        const a = [
                {name:'chandu',id:'27',dept:'CSE'},
                {name:'sudheer',id:'28',dept:'CSE'}
        ];
        
        Object.prototype.tstring = function() {
                return 'Name: '+this.name+', id: '+this.id+', Department: '+this.dept;
        }
        
        document.getElementById("user-0").innerHTML = a[0].tstring();
        document.getElementById("user-1").innerHTML = a[1].tstring();
}

function randomImage() {
        const images = [
                'https://images.unsplash.com/photo-1508138221679-760a23a2285b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=967&q=80',
                'https://images.unsplash.com/photo-1485550409059-9afb054cada4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
                'https://images.unsplash.com/photo-1494253109108-2e30c049369b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
                'https://images.unsplash.com/photo-1493612276216-ee3925520721?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
                'https://images.unsplash.com/reserve/LJIZlzHgQ7WPSh5KVTCB_Typewriter.jpg?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
                'https://images.unsplash.com/photo-1500462918059-b1a0cb512f1d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
                'https://images.unsplash.com/photo-1489533119213-66a5cd877091?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
                'https://images.unsplash.com/photo-1498842812179-c81beecf902c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
                'https://images.unsplash.com/photo-1429087969512-1e85aab2683d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',
                'https://images.unsplash.com/photo-1501426026826-31c667bdf23d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60'
        ];
        var random = images[Math.floor(Math.random() * 10)];
        document.getElementById("rand-images").innerHTML = '<img class="img-fluid" src='+random+' />';
}

window.onload = displayDetails;

