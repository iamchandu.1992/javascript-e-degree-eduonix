var assert = require('chai').assert;
var Validator = require('../build/form.js');

describe("Validations...", function(){
    before(function(){
        this.form = new Validator();
    });
    after(function(){
        delete this.form;
    })
    it("Should return true - Minimum 10 digits for numbers", function() {
        assert.equal(this.form.minimumDigits(7856865675675765), true);
    });

    it("Should return false - Minimum 10 digits for numbers", function() {
        assert.equal(this.form.minimumDigits(65745), false);
    });

    it("Should return true - Minimum 10 digits for alpha numaric", function() {
        assert.equal(this.form.minimumDigits('gcfgbhfdcbfcgghfcv76567fbvc'), true);
    });

    it("Should return false - Minimum 10 digits for alpha numbers", function() {
        assert.equal(this.form.minimumDigits('456fgg'), false);
    });

    it("Should return true - Its numaric value", function() {
        assert.equal(this.form.numaric(7856865675675765), true);
    });

    it("Should return false - Its not numaric value", function() {
        assert.equal(this.form.numaric('657ghf45'), false);
    });

    it("Should return true - Its alpha numaric value", function() {
        assert.equal(this.form.alphanumaric('gcfgbhfdcbfcgghfcv76567fbvc'), true);
    });

    it("Should return false - Its not alpha numaric", function() {
        assert.equal(this.form.alphanumaric('456f$#@gg'), false);
    });
    
})