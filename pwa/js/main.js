var idbApp = (function() {
  'use strict';

  if (!('indexedDB' in window)) {
    console.log('This browser doesn\'t support IndexedDB');
    return;
  }

  var dbPromise = idb.open('dairy', 5, function(upgradeDb) {
    switch (upgradeDb.oldVersion) {
      case 0:
        // a placeholder case so that the switch block will
        // execute when the database is first created
        // (oldVersion is 0)
      case 1:
        console.log('Creating the products object store');
        upgradeDb.createObjectStore('dairy-data', {keyPath: "id", autoIncrement:true});
      case 2:
        var store = upgradeDb.transaction.objectStore('dairy-data');
        store.createIndex('date', 'date', {unique: true});
      case 3:
        var store = upgradeDb.transaction.objectStore('dairy-data');
        store.createIndex('description', 'description');
      case 4:
        var store = upgradeDb.transaction.objectStore('dairy-data');
        store.createIndex('id', 'id');
      case 5:
        upgradeDb.createObjectStore('pictures', {keyPath: "id", autoIncrement : true});
      case 6:
          var store = upgradeDb.transaction.objectStore('pictures');
          store.createIndex('dairyId', 'dairyId', {unique:false});
    }
  });

  function addDairy() {
    dbPromise.then(function(db) {
      var tx = db.transaction('dairy-data', 'readwrite');
      var store = tx.objectStore('dairy-data');
      var items = [{date: document.getElementById('date').value, description: document.getElementById('des').value}];
      return Promise.all(items.map(function(item) {
          return store.add(item);
        })
      ).catch(function(e) {
        tx.abort();
        console.log(e);
      }).then(function() {
        alert('success');
        document.getElementById('date').value = '';
        document.getElementById('des').value = '';
      });
    });
  }


  function showDairy() {
    var s = '';
    dbPromise.then(function(db) {
      var tx = db.transaction('dairy-data', 'readonly');
      var store = tx.objectStore('dairy-data');
      store.index('id');
      return store.openCursor();
    }).then(function showRange(cursor) {
      
      if (!cursor) {return;}

      s += '<h2>' + cursor.value.date + '</h2><p>';
      for (var field in cursor.value) {
        s += field + ' --> ' + cursor.value[field] + '<br/>';
      }
      s += '<button class="btn mt-2" onclick="idbApp.showPictureDiv('+cursor.key+')">Add Picture</button>&nbsp;</p>';
      return cursor.continue().then(showRange);
    }).then(function() {
      if (s === '') {s = '<p>No results.</p>';}
      document.getElementById('dairy-data').innerHTML = s;
    });
  }

  function showPictureDiv(id) {
    let form = "<input type='hidden' value='"+id+"' id='pic-key'>";
    form+= "<input type='text' placeholder='http://example.com/pic.jpg' class='form-control' id='pic-value'>";
    form+="<button class='btn btn-warning mt-2' onclick='idbApp.uploadPic()'>Add Image</button>";
    document.getElementById('add-picture').innerHTML=form;
  }

  function uploadPic() {
    dbPromise.then(function(db) {
      var tx = db.transaction('pictures', 'readwrite');
      var store = tx.objectStore('pictures');
      var items = [{dairyId: document.getElementById('pic-key').value, picture: document.getElementById('pic-value').value}];
      return Promise.all(items.map(function(item) {
          return store.add(item);
        })
      ).catch(function(e) {
        tx.abort();
        console.log(e);
      }).then(function() {
        alert('success');
        document.getElementById('add-picture').innerHTML = '';
      });
    });
    
  }

  // function viewPic(key) {
  //   console.log(key)
  //   //var range = IDBKeyRange.only(key);
  //   var s = '';
  //   dbPromise.then(function(db) {
  //     var tx = db.transaction('pictures', 'readonly');
  //     var store = tx.objectStore('pictures');
  //     var index = store.index('dairyId');
  //     return index.openCursor(IDBKeyRange.only(key));
  //   }).then(function showRange(cursor) {
  //     if (!cursor) {return;}
  //     console.log(cursor.value)
  //     s += '<img src="'+cursor.value.picture+'" />';
  //     return cursor.continue().then(showRange);
  //   }).then(function() {
  //     if (s === '') {s = '<p>No results.</p>';}
  //     document.getElementById('add-picture').innerHTML = s;
  //   });
  // }
  

  return {
    dbPromise: (dbPromise),
    addDairy: (addDairy),
    showDairy:(showDairy),
    showPictureDiv:(showPictureDiv),
    uploadPic:(uploadPic),
    // viewPic:(viewPic),
  };
})();
